package com.gitlab.fisvse.dvop06.domaci_ukol;

public class Location {

	private String name;
	private double x;
	private double y;
	
	public Location(String name, double x, double y) {
		this.setName(name);
		this.setX(x);
		this.setY(y);
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
