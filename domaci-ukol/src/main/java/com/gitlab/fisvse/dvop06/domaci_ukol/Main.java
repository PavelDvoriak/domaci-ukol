package com.gitlab.fisvse.dvop06.domaci_ukol;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Main extends Application
{
	@Override
	public void start(Stage primaryStage) {
	try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/gui.fxml"));
		loader.load();
		HomeController controller = loader.getController();
		VBox root = loader.getRoot();
		Scene scene = new Scene(root, 1000, 700);
		scene.getStylesheets().add(getClass().getResource("/application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		Game game = new Game();
		controller.initialize(game);
		
	} catch (Exception e) {
		e.printStackTrace();
	}
}

	public static void main(String[] args) {
		
		if (args.length == 0) {
			
			launch(args);
		} else {
			System.out.println("An error has occured.");		
		}
		
	}

}
