package com.gitlab.fisvse.dvop06.domaci_ukol;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Game {

	private List<Location> locations;
	private int points;
	private boolean gameOver;
	private double maxDistance = 30;
	
	public Game() {
		locations = new ArrayList<>();
		locations.add(new Location("Příbram", 239.00, 345.00));
		locations.add(new Location("Olomouc", 614.00, 361.00));
		locations.add(new Location("Teplice", 221.00, 184.00));
		locations.add(new Location("Bruntál", 655.00, 304.00));
		locations.add(new Location("Chrudim", 455.00, 303.00));
		locations.add(new Location("Náchod", 482.00, 220.00));
		locations.add(new Location("Havlíčkův Brod", 425.00, 359.00));
		locations.add(new Location("Sokolov", 96.00, 260.00));
		locations.add(new Location("Český Brod", 341.00, 280.00));
		locations.add(new Location("Mikulov", 547.00, 485.00));
		
		Collections.shuffle(locations);
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
	public Location getCurrentLocation() {
		if(locations.isEmpty()) {
			setGameOver(true);
		}
		
		return locations.get(0);
	}
	
	public void removeCurrentLocation() {
		if(locations.size() == 1) {
			setGameOver(true);
		}
	}
	
	public double getDistance(double x, double y, double a, double b) {
		return Math.sqrt(Math.pow((x - a), 2) + Math.pow(y - b, 2));
	}
	
	public boolean checkDistance(double a) {
		if(a <= maxDistance) {
			return true;
		} return false;
	}
	public void addPoints() {
		points++;
	}
	
}
