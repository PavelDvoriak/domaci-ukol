package com.gitlab.fisvse.dvop06.domaci_ukol;

import java.util.Observable;
import java.util.Observer;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class HomeController implements Observer {

	@FXML ImageView img;
	@FXML TextArea output;
	
	private Game game;
	
	public void initialize(Game g) {
		game = g;
		
		output.setText("Location to find: " + game.getCurrentLocation().getName() + "\n\n" + "Your points: " + game.getPoints());
		
		img.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			
			@Override
			public void handle(MouseEvent event) {
				Location location = game.getCurrentLocation();
				
				double distance = game.getDistance(location.getX(), location.getY(), event.getX(), event.getY());
				
				if(game.checkDistance(distance)) {
					output.appendText("\n\n");
					output.appendText("You got it!");
					game.addPoints();
				} else {
					output.appendText("\n\n");
					output.appendText("Not even close, sorry.");
				}
				game.removeCurrentLocation();
				updateGUI();
				
				event.consume();
			}
		});
		
	}

	public void updateGUI() {
		Location location = game.getCurrentLocation();
		
		if(game.isGameOver()) {
			output.setText("The game is over, thank you for playing." + "\n" + "You have scored " + game.getPoints() + " points.");
		}
		
		output.setText("Location to find: " + game.getCurrentLocation().getName() + "\n\n" + "Your points: " + game.getPoints());
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}

}
